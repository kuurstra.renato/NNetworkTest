// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "NettyBrainPrivatePCH.h"

#include "NettyStaticLibrary.h"
//#include "MyNettyBrainObject.h"
#include "GenePoolObject.h"



class FNettyBrain : public INettyBrain
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

IMPLEMENT_MODULE( FNettyBrain, NettyBrain )



void FNettyBrain::StartupModule()
{
	// This code will execute after your module is loaded into memory (but after global variables are initialized, of course.)
}


void FNettyBrain::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}



