
#include "NettyBrainPrivatePCH.h"
#include "NettyStaticLibrary.h"

UNettyStaticLibrary::UNettyStaticLibrary(const FObjectInitializer& ObjectInitializer)
		: Super(ObjectInitializer)
{

}

template <typename T>
std::vector<T> UNettyStaticLibrary::FromTArrayToVector(TArray<T> Array)
{
	std::vector<T> ret_value;

	ret_value.resize( Array.Num() );

	for (size_t i = 0; i < Array.Num(); i++)
	{
		ret_value[i] = Array[i];
	}

	return ret_value;
}

template <typename T>
TArray<T> UNettyStaticLibrary::FromVectorToTArray(std::vector<T> vector)
{
	TArray<T> ret_value;
	size_t vectorsize = vector.size();
	ret_value.SetNum(vectorsize);

	for (size_t i = 0; i < vectorsize; i++)
	{
		ret_value[i] = vector[i];
	}

	return ret_value;
}
