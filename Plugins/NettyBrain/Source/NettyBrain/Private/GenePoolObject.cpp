
#include "NettyBrainPrivatePCH.h"
#include "GenePoolObject.h"


UNettyGenePool::UNettyGenePool(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

	
void UNettyGenePool::Init(TArray<int> netshape, int maxpop, int pop, bool islstm, bool israndomdist, bool isbias)
{
	m_NetShape = netshape;
	m_MaxPop = maxpop;
	m_Pop = pop;
	m_IsLSTM = islstm;
	m_IsRandomDist = israndomdist;
	m_IsBias = isbias;
}


void UNettyGenePool::SetMutBreed(float MutationRate, float BreedRate, float RawFitness)
{

	Population.SetGeneticVariables( RawFitness, BreedRate, MutationRate);
}

void UNettyGenePool::CreateNewPopulation()
{
	Population.Init(UNettyStaticLibrary::FromTArrayToVector(m_NetShape), m_MaxPop, m_Pop, m_IsLSTM, m_IsRandomDist, m_IsBias);
}

int UNettyGenePool::GetNewSolution()
{
	return Population.GetNewSolution();
}

TArray<float> UNettyGenePool::EvaluateSolution(int SolutionIndex, TArray<float> Inputs)
{
	return UNettyStaticLibrary::FromVectorToTArray(
			Population.Eval_Solution(SolutionIndex, UNettyStaticLibrary::FromTArrayToVector(Inputs)) );
}

TArray<float> UNettyGenePool::EvaluateTrainNetwork(int networkindex, int traintype, float learnrate, TArray<float> Inputs, TArray<float> OptimalOutputs)
{
	return UNettyStaticLibrary::FromVectorToTArray( Population.Eval_Solution_BackProp(networkindex, UNettyStaticLibrary::FromTArrayToVector(Inputs), traintype, UNettyStaticLibrary::FromTArrayToVector(OptimalOutputs), learnrate ) );
}

int UNettyGenePool::TrimSolutions()
{
	return Population.TrimSolutions();
}

void UNettyGenePool::KillSolution(int SolutionIndex, float fitness)
{
	Population.KillSolution(SolutionIndex, fitness);
}

void UNettyGenePool::ClearElite()
{
	Population.ClearElite();
}


void UNettyGenePool::SetFitness(int Solution, float Fitness)
{
	Population.SetRawFitness( Solution, Fitness);
}

float UNettyGenePool::GetTotalFitness()
{
	return Population.GetTotalFitness();
}

void UNettyGenePool::SetBreedOperator( int BreedOperator )
{
	Population.SetBreedOperator(BreedOperator);
}

void UNettyGenePool::SetMutationOperator(int MutationOperator)
{
	Population.SetMutationOperator(MutationOperator);
}

void UNettyGenePool::PrintNetworkToCsv(int solution, FString Filename)
{
	Population.PrintNet(solution, std::string(TCHAR_TO_UTF8(*Filename)));
}


