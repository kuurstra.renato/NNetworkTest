
#pragma once

#include <vector>
#include "NettyStaticLibrary.generated.h"

UCLASS()
class UNettyStaticLibrary : public UObject
{
	GENERATED_BODY()

public:


	UNettyStaticLibrary(const FObjectInitializer& ObjectInitializer);

	template<typename T>
	static FORCEINLINE std::vector<T> FromTArrayToVector(TArray<T> Array);

	template<typename T>
	static FORCEINLINE TArray<T> FromVectorToTArray(std::vector<T> vector);


};