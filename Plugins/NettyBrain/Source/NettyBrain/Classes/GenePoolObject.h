#pragma once

//#include "../../ThirdParty/NeuralNetworksStatic/Includes/NNetwork.h"
#include "../../ThirdParty/NeuralNetworksStatic/Includes/GA_Trainer.h"

#include "NettyStaticLibrary.h"
#include "GenePoolObject.generated.h"

/*
	*    Genetic Pool Object. It will populate a pool of neural networks, and then will give 
	*    a network back anytime is asked to do so.
*/
UCLASS(Blueprintable)
class UNettyGenePool : public UObject
{
	GENERATED_BODY()

	GA_Trainer Population;
	bool IsPopulationInitiated = false;

public:

	UPROPERTY(EditAnywhere)
	TArray<int> m_NetShape;

	UPROPERTY(EditAnywhere)
	int m_MaxPop;
	UPROPERTY(EditAnywhere)
	int m_Pop;
	UPROPERTY(EditAnywhere)
	bool m_IsLSTM;
	UPROPERTY(EditAnywhere)
	bool m_IsRandomDist;
	UPROPERTY(EditAnywhere)
	bool m_IsBias;

	UNettyGenePool(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Init population Variables", Keywords = "init params"), Category = "NeuralNetworks genetic algorithm")
	void Init(TArray<int> netshape, int maxpop, int pop, bool islstm, bool israndomdist, bool isbias);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Setup genetic parameters", Keywords = "set params genetic"), Category = "NeuralNetworks genetic algorithm")
	void SetMutBreed( float MutationRate, float BreedRate, float RawFitness);


	UFUNCTION(BlueprintCallable, meta = (DisplayName = "create a population of solutions", Keywords = "create a new population of solutions"), Category = "NeuralNetworks genetic algorithm")
	void CreateNewPopulation();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Breed new Solution", Keywords = "new solution breed mutate"), Category = "NeuralNetworks genetic algorithm")
	int GetNewSolution();

	/*
	* Set The inputs and evaluate the network at the index.

	@SolutionIndex - the index of the solution inside the genetic algorithm population
	@Inputs - The floating point TArray containing the inputs values
	*/
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "set network inputs and evaluate it", Keywords = "input layer evaluate network"), Category = "NeuralNetworks genetic algorithm")
	TArray<float> EvaluateSolution(int SolutionIndex, TArray<float> Inputs);

	/*
	Will evaluate the network of a certain ID. It will do a backpropagation using the type requested.
	It will output the values that then will be used.
	*/
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "evaluate and train network with key", Keywords = "evaluate train network"), Category = "NeuralNetworks")
	TArray<float> EvaluateTrainNetwork(int networkindex, int traintype, float learnrate, TArray<float> Inputs, TArray<float> OptimalOutputs);

	/*
	* Check if the solutions not in play(history) are > than max pop. If so it will destroy the excess, from the oldest onward

	*/
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Trim solutions history", Keywords = "trim solutions"), Category = "NeuralNetworks genetic algorithm")
	int TrimSolutions();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Kill Individual", Keywords = "Kill individual destroy"), Category = "NeuralNetworks genetic algorithm")
	void KillSolution(int SolutionIndex, float fitness);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Clear Elite Table", Keywords = "clear elite"), Category = "NeuralNetworks genetic algorithm")
	void ClearElite();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Fitness to solution", Keywords = "set fitness solution"), Category = "NeuralNetworks genetic algorithm")
	void SetFitness(int Solution, float Fitness);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "get total fitness", Keywords = "get total fitness"), Category = "NeuralNetworks genetic algorithm")
	float GetTotalFitness();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set breed operator", Keywords = "set breed operator"), Category = "NeuralNetworks genetic algorithm")
	void SetBreedOperator(int BreedOperator);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Mutation operator", Keywords = "set mutation operator"), Category = "NeuralNetworks genetic algorithm")
	void SetMutationOperator(int MutationOperator);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Print Network to csv", Keywords = "print network csv"), Category = "NeuralNetworks genetic algorithm")
	void PrintNetworkToCsv(int solution, FString Filename);

};