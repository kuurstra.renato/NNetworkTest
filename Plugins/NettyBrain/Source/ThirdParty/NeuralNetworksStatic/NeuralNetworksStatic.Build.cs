// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class NeuralNetworksStatic : ModuleRules
{
	public NeuralNetworksStatic(TargetInfo Target)
	{
		Type = ModuleType.External;

		if (Target.Platform == UnrealTargetPlatform.Win64)
		{
            // Add the import library
            PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "./Libraries"));
            //PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "../../ThirdParty/NeuralNetworksStatic/Libraries"));
            PublicAdditionalLibraries.Add("NeuralNetworksStatic.lib");

            // Delay-load the DLL, so we can load it from the right place first
            //PublicDelayLoadDLLs.Add("NeuralNetworksStatic.dll");

            PublicDelayLoadDLLs.Add("NettyBrain.dll");
        }
/*        else if (Target.Platform == UnrealTargetPlatform.Mac)
        {
            PublicDelayLoadDLLs.Add(Path.Combine(ModuleDirectory, "Mac", "Release", "libExampleLibrary.dylib"));
        } */
	}  
}
