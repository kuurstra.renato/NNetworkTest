#pragma once

#include <vector>
#include <iterator>
#include "Neuron.h"

class NNetwork
{

public:
	NNetwork();
	~NNetwork();
	/*
		Print a network to a csv file.
		@param A  - Network to be printed in a csv file.
		@param FileName - String of the filename, extension will be added a second time
		@param close - Close the file once finished?
	*/
	static void PrintNetworkCsv( NNetwork* A, std::string FileName, bool close = false);

	/*
	*  Linear interpolation of two network. The first network weights will be changed based on the value of B and alpha
	@param A - This network weights will be changed based on the result of a interpolation of A and B, over alpha value
	@param B - the second network, this will not be affected
	@param alpha - the alpha for the linear interpolation
	*/
	static void LerpNetworks(NNetwork* A, NNetwork* B, float alpha);

	/*
	*  Will randmly copy one neuron from solution B inside solution A
	@param A - This network weights will be randomly swapped with the second
	@param B - the second network, this will not be affected
	@param alpha - how much probabilities has a neuron to be swapped
	*/
	static void OneRandomNeuron(NNetwork* A, NNetwork* B, float alpha);

	/*
		*  Copy network B weights inside network A
		@param A - His weights changed to that of B
		@param B - This will not changing at all
	*/
	static void CopyNetworks(NNetwork* A, NNetwork* B);

	int ID;

	//Init The network shapes according to specified values
	void shape_init(std::vector<int> NetWorkShape, bool isbiased, bool islstm);

	/*
	* Form the synapsis of the neurons, linking them togheder and randmly assigning weights.
	@param HasDistrubRandom - Calulate the weights with values depending to the number of neurons  EXPERIMENTAL
	*/
	void synapse_init(bool HasDistrubRandom);

	bool SetInputLayer(std::vector<float> inputs);

	std::vector<float> evaluate();

	void BackPropagation( int type, std::vector<float> output_ideal, float learnrate );

	void Mutate(float rate, int type, float str);

	std::vector<int> GetShape();

	std::vector<float> LastIdealOutputs;

protected:



private:

	//Network states
	bool IsInitialized = false;
	bool IsBiased = false;
	bool IsLSTM = false;

	std::vector< std::vector<Neuron*> > NeuralNetwork;

};