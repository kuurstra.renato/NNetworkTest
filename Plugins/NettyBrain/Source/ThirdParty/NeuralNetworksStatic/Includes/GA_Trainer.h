#pragma once

/*
TODO:
1) Multi threads
2) Set input on a array of solutions
3) iterate a array of solutions
4) Only living count toward fitness
5) Averange fitness
6) Elitarism
*/
#include <list>
#include <vector>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <sstream>
#include "LogsKeeper.h"
#include "MatAndRandom.h"
#include "Neuron.h"
#include "NNetwork.h"

class GA_Trainer
{
protected:

public:

	/*
	Structs
	*/
	struct Solution
	{
		int SolutionIndex;
		bool IsElite = false;
		std::list<Solution*>::iterator position;

		NNetwork* network;
		// The raw fitness of the solution, when a solution is made it will be defaulted according to the trainer settings(usually 0.01f)
		float raw_fitness = 0.0f;

		Solution();
		~Solution();
	};

	GA_Trainer();
	~GA_Trainer();

	/*
	* Never ever try to change this static values during execution. To change them, re-init the trainer clearing it
	*/

	std::vector<int> NetsShape;
	bool IsBias;
	bool IsDistrib;
	bool IsLSTM;


	void Init(std::vector<int> shape, int maxpopulation, int initialpopulation, bool islstm, bool israndomdist, bool isbias);

	void SetGeneticVariables( float rawfitness, float breedrate, float mutationrate);

	void PopulateSolutions( int NIndividuals, bool IsResetting );

	int CreateSolution();

	int CopyIndividual(int solutionindex);

	/**
		Evaluate selected solution, returning vector of outputlayer
		@param solutionindex - The index of the network, to consult indexmap, and get the correct iterator in the list of solutions
		@param inputlayer - The input values for the network. ** if the size is different.. we see what will happen ;)
	*/
	std::vector<float> Eval_Solution( int solutionindex, std::vector<float> inputlayer );

//<<<<<<< HEAD
	/**
		Set the rawfitness for the solution ad index
		@param solutionindex - The index of the solution inside the iterator map
=======

	/*
	* Evaluate selected solution, returning vector of outputlayer and doing back propagation
	@param solutionindex - The index of the network, to consult indexmap, and get the correct iterator in the list of solutions
	@param inputlayer - The input values for the network. ** if the size is different.. we see what will happen ;)
	@param type - Type of backpropagation ( 0 = Made by Ren� )
	@param output_ideal - The ideal output we expect the network to conform
	@param learnrate - Learning rate for the backpropagation
	*/
	std::vector<float> Eval_Solution_BackProp(int solutionindex, std::vector<float> inputlayer, int type, std::vector<float> output_ideal, float learnrate);

	/*
		Print on .csv file the network.
	*/
	void GA_Trainer::PrintNet(int solutionindex, std::string filename);

	/*
	* Set the rawfitness for the solution ad index
	@param solutionindex - The index of the solution inside the iterator map
>>>>>>> a44b48be2c53a516a983ef10340cb7e9c0f748dd
	*/
	void SetRawFitness(int solutionindex, float rawfitness);

	/**
		Add the rawfitness for the solution ad index
		@param solutionindex - The index of the solution inside the iterator map
	*/
	void AddRawFitness(int solutionindex, float rawfitness);

	/**
		Roulette wheel selection of a Solution, based on fitness		
		@param startat - The index of the solution inside the iterator map
	*/
	std::pair<int, float> RouletteSelection();

	/**
		Get solution positioned at fitness value, according to list of elites, solution and the fitness required
		@param solutionatposition - The float value that will considered to find the proper solution
	*/
	int GetSolutionAtFitness(float solutionatposition);

	/**
		Get a new solution, breeding and mutating according to settings
	*/
	int GetNewSolution();

	/**
		Breed the solution with another if chance allow it
	*/
	int Breed(int solution, float rouletteposition);

	/**
		Mutate the network according to mutation operator using str if needed
		check MutationOperator variable
	*/
	void Mutate(int solution, int mutationoperator, float str);

	/**
		Remove solution from "in use" status, also update the fitness of it
		It will check for insertion on the elite table.
	*/
	void KillSolution(int solution, float fitness);

	/**
		Clear Elite table
	*/
	void ClearElite();

	/**
		Will destroy any unused solution if Solutions.size > MaxSolutions
	*/
	int TrimSolutions();

	/**
		Recalc and give total fitness, only counted if the solutions are individual
	*/
	float GetTotalFitness();

	/**
		Set the operation for breeding:
		0 = Lerp
		1 = Select a neuron at random
	*/
	void SetBreedOperator(int breedoperator);

	/**
		Set the operator for mutation:
		0 = Random mutation of weight in the form of -1 +1
		1 = The weight get added a value of his approximate 30%, multiplied by factor str and with random sign +-
	*/
	void SetMutationOperator(int mutationoperator);

	std::pair< int, NNetwork*> GetNetwork( int solution );

protected:

	/**
		Settings:
	*/
	int MaxSolutions = 100;

	// This value will be the default value of solutions.
	// It is required for the roulette selection to work properly(cant be zero, or negative)
	float BaseRawFitness = 0.01f;

	/**
		%(0-1) rapresenting the chance of breeding of the solution
	*/
	float BreedRate = 1.0f;

	/**
		Breeding Type
		0: Lerp
		1: One Neuron at random
	*/
	int BreedOperator = 1;

	/**
		Mutation Operator
		0: Complete random weight, normal values
		1: Add a positive/negative number, using a str factor gained from fitness
	*/
	int MutationOperator = 1;

	/**
		%(0-1) rapresenting the chance of breeding of the solution
	*/
	float MutationRate = 0.01f;


	// Current MaxIndex reached
	int MaxSolutionIndex = 0;

	// This value will store the total accumulated fintess inside the list of solutions
	float TotalFitness = 0.0f;

	float MaxFitness = 0.0f;

	float AvrgFitness = 0.0f;

	// List because we need to be able to erase underperforming solutions ( low fitness ) while traversing the list
	// maintaining also the pointer to the position in the list, so we can always refer to the same element even outside
	std::list< Solution* > Solutions;

	// A unordered map, used to let outside application deal with this application using simple integers
	std::unordered_map< int, std::list<Solution*>::iterator> SolutionsIndexs;

	// Alive Solutions
	std::unordered_set< int > SolutionInUse;

	// Elite Solutions
	struct EliteTable {
		float MinRawFitness = 0.0f;
		float TotalEliteFitness = 0.0f;
		int ElitesMax = 5;
		
		int ElitesSize = 0;
		std::map< float, int> EliteSolutions;
		
		EliteTable();
		~EliteTable();

		std::pair<int, bool> CheckToInsert(int solution, float fitness);
		int RouletteWheel(float roulettevalue);
		std::vector<int> Clear();

	} Elites;

	/*
		Utility functions
	*/
	void LerpSolutions( Solution* A, Solution* B);
	void OneRandomNeuron( Solution* A, Solution* B);
	void RecalcTotalFitness();
	void RemoveFromElite(std::vector<int> solutions);

private:
//	void destroy_solution(int solution);
	auto destroy_solution(std::list<Solution*>::iterator it);
};

