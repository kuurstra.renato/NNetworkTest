
#pragma once

#include <vector>
#include <queue>
#include <sstream>
#include "MatAndRandom.h"
#include "LogsKeeper.h"


/*
Type of neurons:

Neuron			-- Normal neuron, base functionality
Neuron_Input		-- Input neuron.
*/

class Neuron {
public:
	Neuron(){}
	Neuron(int layer, int posinlayer );
	~Neuron();
//	Neuron( int Enum_SquashType );

	virtual float get_value();
	void set_value(float val);
	virtual void set_LastValidValue();

	float get_error();
	void set_error( float val );
	void set_LastValidError();

	int get_posinlayer();

	std::vector<float> GetWeights();

	//will reset all weight randomizing them with values from -1 to 1
	// If Distribution is set yes, the weights will be in range depending on how many weights we have to generate
	virtual void randomize_weights(bool distrib_control);

	// Create the synapsis
	//	virtual void synapsis(Neuron* previouslayer[]);
	virtual void synapsis(std::vector< Neuron* > previouslayer, std::vector< Neuron* > nextlayer);

	//Used to calculate the cost
	float NeuronCost;

	enum SquashFormula {
		Tanh = 0,
		Tanh_Custom = 1,
		Tanh_Normalized = 2
	} SquashType; // not really used yet

	const float MutationFactor = 0.3f;

	// All Evaluate functions will return the result, plus setting old value as PreviousValue, and setting the result
	// of evaluate as Value
	virtual float evaluate_tanh();
	//best 1.2
	float evaluate_tan_custom(float tanh_factor);

	// normal tanh, then add 1, and divide by 2(getting a 0 - 1.0 value)
	float evaluate_tanh_norm();

	virtual void LerpNeuron( Neuron* B, float alpha );
	virtual void CopyNeuronWeights(Neuron* B);

	/* Back Propagation */
	enum EBPType {
		SimpleBP
	};
	virtual void BackPropagation( int type, float output_ideal, float learnrate );
	/*
		***********************************************************************
	*/

	virtual void Mutate(float rate, int type, float str);

protected:
	int Layer;
	int PosInLayer; // Position in current layer.

	float LastValidValue = 0.0;  // This value is only set when the whole layer manipulation is complete
								 // the corresponding method is called by implementation of neural network
	float Value = 0.0;			 // this value will store the current elaborated value
	float PreviousValue = 0.0;   // this will store the previous LAstValidValue

	float LastValidError = 0.0; // Partial derivate of error * Partial derivate of logistic function
								// Only set when the layer has finished evluation.
	float Error = 0.0; // this value will store the current elaborated value
	float PreviosError = 0.0; 

	//Maximum number of values
	int MaxValues = 2;

	// Weighted connections to all the other neurons
	// We store a reference to the other neurons, and relative weight of those neurons
	std::vector < Neuron* > Connections;
	std::vector < float > Weights;

	std::vector < Neuron* > BackConnections;
	//std::pair< std::vector<Neuron*>, std::vector<float> > WeightedConnections;

private:

};

class Neuron_Input : public Neuron
{
public:
	Neuron_Input( int layer, int posinlayer) : Neuron( layer, posinlayer){}
	void Mutate(float rate, int type, float str) override {}
	void CopyNeuronWeights(Neuron* B) override {}
	void randomize_weights(bool distrib_control) override {}
	void synapsis(std::vector< Neuron* > previouslayer, std::vector< Neuron* > nextlayer) override {}
	void BackPropagation(int type, float output_ideal, float learnrate) override {}
};

class Neuron_Bias : public Neuron
{
public:
	Neuron_Bias(int layer, int posinlayer) : Neuron(layer, posinlayer) {}
	//Overrided in header since code is simple
	void synapsis(std::vector< Neuron* > previouslayer, std::vector< Neuron* > nextlayer) override {}
	void randomize_weights(bool distrib_control) override {}
	void CopyNeuronWeights(Neuron* B) override {}
	float evaluate_tanh() override
	{
		Value = PreviousValue = LastValidValue = 1.0f;
		return 1.0f;
	}
	float get_value() override { return 1.0f; }
	void Mutate(float rate, int type, float str) {}

	void BackPropagation(int type, float output_ideal, float learnrate) override {}
};

class Neuron_Gate : public Neuron
{

};
/*    Neuron */

class Neuron_LSTM : public Neuron
{
public:
	Neuron_LSTM(int layer, int posinlayer) : Neuron(layer, posinlayer) {}

	/*
	* Form the connections of the neuron. Keep in mind that this type of neuron will reiterate the same to other
	* three neurons, known as input/forget/output gates.

	@param PrevPlusCurrent - it's a vector of neurons, composed by the previous layer neurons, and current layers.

	*/
	void synapsis( std::vector< Neuron* > PrevPlusCurrent, std::vector< Neuron* > nextlayer) override;

	void randomize_weights(bool distrib_control) override;

	float evaluate_tanh() override;

	void LerpNeuron(Neuron* B, float alpha) override;
	void CopyNeuronWeights(Neuron* B) override;

	void Mutate(float rate, int type, float str);

	// This store the value in the neuron, it's changed by summing the result of the input layer * input gate value
	double ErrorCarousel = 0.0;

protected:
	// LSTM Specifics: NEuronsInSameLayer is used to split the PrevPlusCurrent vector of synapsis   NOT USED YET
	//int NeuronsInSameLayer = 0;

	// LSTM details of current layer weights
	Neuron_Gate InputGate;
	Neuron_Gate ForgetGate;
	Neuron_Gate OutputGate;
};
//
//class Neuron_Gate : public Neuron
//{
//};