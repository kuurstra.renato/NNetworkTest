#pragma once

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <random>
#include <chrono>

class MatAndRandom
{
public:

	static float lerpf(float a, float b, float alpha);

	static MatAndRandom& getInstance()
	{
		static MatAndRandom    instance; // Guaranteed to be destroyed.
							  // Instantiated on first use.
		return instance;
	}
private:
	MatAndRandom()
	{
		Rng.seed(RandomDevice());
	}                    // Constructor? (the {} brackets) are needed here.


	std::mt19937_64 Rng;
	std::random_device RandomDevice;

							  // C++ 03
							  // ========
							  // Dont forget to declare these two. You want to make sure they
							  // are unacceptable otherwise you may accidentally get copies of
							  // your singleton appearing.
//	MatAndRandom(MatAndRandom const&);              // Don't Implement
//	void operator=(MatAndRandom const&); // Don't implement

							  // C++ 11
							  // =======
							  // We can use the better technique of deleting the methods
							  // we don't want.
public:
	MatAndRandom(MatAndRandom const&) = delete;
	void operator=(MatAndRandom const&) = delete;

	void ReSeed( unsigned Seed);

	int intn_n(int min, int max);
	double rean_n(double min, double max);
	double rea0_1();
	double rea1_1();
	int sign_1_1();

};